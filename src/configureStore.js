import { applyMiddleware, createStore, combineReducers } from 'redux';
import checkoutReducer from './Checkout/reducer';
import logger from 'redux-logger'

const initialState = {
  checkOut : {
    products : [
      {id : 1, price: '49.99', name: 'Mountain Dew',  quantity : 1},
      {id : 2, price: '9.99',  name: 'Desperados',  quantity : 2},
      {id : 3, price: '19.99',  name: 'Jack Daniels',  quantity : 3}
    ]
  }  
}

const store = createStore(
  combineReducers({
    checkOut: checkoutReducer
  }),
  initialState,
  applyMiddleware(logger)
);

export default store;