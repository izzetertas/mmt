import React, { Component } from 'react';
import { connect } from 'react-redux';
import { removeItem, removeAllItem, changeQuantity } from './actions';
import CheckoutFooter from './footer';

class Checkout extends Component {
	changeQuantity = (id, quantity) => {
		const product = this.props.products.filter(i => i.id === id);
		if (quantity > 0) {
			this.props.change(id, quantity);
		} else {
			this.props.remove(id);
		}
	}

	handleClear = () => {
		this.props.removeAll();
	}

  render() {
		const { products } = this.props;
		let totalPrice = 0;
		let rows = null;
		if (products && products.length > 0) {
			totalPrice = products.reduce((acc, p) => acc + (p.quantity * parseInt(p.price)), 0 ).toFixed(2);
			rows = products.map(product => (
				<div key={product.id} className="product-item">
					<span className="product-column">
						{product.name}
					</span>
					<span className="product-column">
						<input
							defaultValue={product.quantity}
							onBlur={(e) => this.changeQuantity(product.id, e.target.value)} 
					/>
					</span>
					<span className="product-column">
						<strong>{product.quantity * parseInt(product.price)} €</strong>
					</span>
					<span className="product-column">
						<button onClick={() => this.changeQuantity(product.id, 0)}>X</button>
					</span>
				</div>
			));
		} 

    return (
			<div>
				<div className="shopping-cart-list">
					{rows}
				</div>
				<CheckoutFooter
					total={totalPrice}
					onClearClick={this.handleClear} />
			</div>
		);
  }
}

const mapStateToProps = state => ({
	products : state.checkOut.products
});

const mapDispatchToProps = dispatch => ({
  remove: (id) => dispatch(removeItem(id)),
  removeAll: () => dispatch(removeAllItem()),
	change: (id, quantity) => dispatch(changeQuantity(id, quantity))
});

export default connect(mapStateToProps, mapDispatchToProps)(Checkout);
