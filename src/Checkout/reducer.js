import { REMOVE_ITEM, REMOVE_ALL_ITEM, CHANGE_QUANTITY } from './constants';

const initialState = { 
  products : []
};

export default function reducer(state = initialState, action ) {
  switch (action.type) {
    case REMOVE_ITEM:
      let products = state.products.filter(item => item.id !== action.id); 
      return {
        products
      };
    case REMOVE_ALL_ITEM:
      return {
        products : []
      };
    case CHANGE_QUANTITY: 
      products = state.products.map(item => {
        if (item.id === action.payload.id) {
          return {...item, quantity : action.payload.quantity}
        } else {
          return {...item};
        }
      });
      return { 
        products 
      };
    default:
      return state;
  }
} 