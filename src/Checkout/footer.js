import React, { Component } from 'react';
import PropTypes from 'prop-types';

const CheckoutFooter = ({ total, onClearClick, onCheckOutClick }) => {
if(total === 0) {
	return <div className="checkout-footer">There is no item on the basket</div>
}
return (
	<div className="totalPrice">
		<span>Total : {total}</span>
		<span>€</span>
		<button onClick={() => onClearClick()}>Clear</button>
		<button onClick={() => onCheckOutClick()}>Checkout</button>
	</div>
)
};

CheckoutFooter.propTypes = {
	total : PropTypes.number.isRequired
}
export default CheckoutFooter;
